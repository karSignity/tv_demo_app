package com.dummy.catalano.myapplication;

import java.io.File;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

interface ApiInterface {
    /***
     * upload pic
     * */
    @Multipart
    @POST("/files/upload")
    Call<ResponseBody> updatePic(@Part MultipartBody.Part image);

    @GET("/images/getLatestFile")
    Call<ResponseBody> getRecentFile();


}

package com.dummy.catalano.myapplication;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.Image;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dummy.catalano.myapplication.Urls.IMAGE_TYPE;

public class BannerActivity extends AppCompatActivity {

    ImageView banner;
    MutedVideoView videoView;
    Runnable runnable;
    Handler handler;
    private int inveral = 1;
    MediaController mediaControls;
    private LinearLayout progressBarRegion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner);
        progressBarRegion = findViewById(R.id.progress_bar_region);
        videoView = findViewById(R.id.video_id);
        banner = (ImageView) findViewById(R.id.banner);
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                // your code while refreshing activity
                Toast.makeText(BannerActivity.this, "Refreshing", Toast.LENGTH_SHORT).show();
                getRecentFile();
                // handler.postDelayed(runnable, inveral * 60 * 1000);
                handler.postDelayed(runnable, inveral * 60 * 1000);
            }
        };
        handler.postDelayed(runnable, 0);

        getRecentFile();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.one:
                inveral = 1;
                break;
            case R.id.five:
                inveral = 5;
                break;
            case R.id.ten:
                inveral = 10;
                break;
            case R.id.twenty:
                inveral = 20;
                break;
        }
        return true;
    }

    /**
     * Method to set volume of the media player
     *
     * @param amount
     */
    private void setVolume(int amount, MediaPlayer mediaPlayer) {

        try {
            final int max = 100;
            final double numerator = max - amount > 0 ? Math.log(max - amount) : 0;
            final float volume = (float) (1 - (numerator / Math.log(max)));

            mediaPlayer.setVolume(volume, volume);
        } catch (IllegalStateException ie) {
            ie.printStackTrace();
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }
    }

    public void getRecentFile() {
        Call<ResponseBody> call = ApiClient.getInstance().getApiService().getRecentFile();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    String fileName = response.headers().get("fileName");
                    if (fileName.contains(IMAGE_TYPE)) {
                        banner.setVisibility(View.VISIBLE);
                        videoView.setVisibility(View.GONE);
                        if (videoView != null && videoView.isPlaying()) {
                            videoView.stopPlayback();
                        }
                        //Image
                        String imgurl = "http://112.196.1.221:8700/images/getLatestFile";
                        //Picasso.with(BannerActivity.this).load(imgurl).placeholder(R.drawable.ic_loading).error(R.drawable.ic_error).memoryPolicy(MemoryPolicy.NO_CACHE).into(banner);
                        Glide.with(BannerActivity.this)
                                .load(imgurl)
                                .centerCrop()
                                .placeholder(R.drawable.loading).error(R.drawable.ic_error).diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(banner);
                    } else {
                        //Video
                        playVideo();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void playVideo() {
        banner.setVisibility(View.GONE);
        videoView.setVisibility(View.VISIBLE);
        if (mediaControls == null) {

            mediaControls = new MediaController(BannerActivity.this);
            mediaControls.setAnchorView(videoView);
        }
        String imgurl = "http://112.196.1.221:8700/images/getLatestFile";
        videoView.setVideoURI(Uri.parse(imgurl));
        mediaControls.setAnchorView(videoView);
        videoView.setMediaController(mediaControls);
        videoView.start();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            videoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                @Override
                public boolean onInfo(MediaPlayer mediaPlayer, int what, int extra) {
                    setVolume(100, mediaPlayer);
                    switch (what) {
                        case MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START: {
                            progressBarRegion.setVisibility(View.GONE);
                            return true;
                        }
                        case MediaPlayer.MEDIA_INFO_BUFFERING_START: {
                            progressBarRegion.setVisibility(View.VISIBLE);
                            return true;
                        }
                        case MediaPlayer.MEDIA_INFO_BUFFERING_END: {
                            progressBarRegion.setVisibility(View.GONE);
                            return true;
                        }
                        case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
                            break;
                    }
                    return false;
                }
            });
        }

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Toast.makeText(getApplicationContext(), "Thank You...!!!", Toast.LENGTH_LONG).show();
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(getApplicationContext(), "Oops An Error Occur While Playing Video...!!!", Toast.LENGTH_LONG).show();
                return false;
            }
        });
    }
}

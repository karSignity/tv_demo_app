package com.dummy.catalano.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.Permission;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dummy.catalano.myapplication.Urls.IMAGE_TYPE;
import static com.dummy.catalano.myapplication.Urls.VIDEO_TYPE;

public class MainActivity extends AppCompatActivity {

    // private static int RESULT_LOAD_IMAGE = 1;
    private File file;
    private ImageView imageView, imageView1;
    private VideoView videoView;
    private static final int REQUEST_CODE = 819;
    private static final int VIDEO_REQUEST = 137;
    private static final int GIF_REQUEST = 138;
    private LinearLayout progressBarRegion;
    String[] imageExtensions = new String[]{"TIFF", " JPEG", "JPG", "GIF", "PNG"};
    String[] videoExtensions = new String[]{"mp4", "m4a", "m4v", "f4v", "f4a", "m4b", "m4r", "f4b", "mov", "3gp", "3gp2", "3g2", "3gpp", "3gpp2"
            , "ogg", "oga", "ogv", "ogx", "wmv", "wma", "asf*", "webm", "flv", "AVI"};


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (ImageView) findViewById(R.id.imgView);
        //imageView1 = (ImageView) findViewById(R.id.imgView1);
        videoView = (VideoView) findViewById(R.id.videoview);
        progressBarRegion = findViewById(R.id.progress_bar_region);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
        }

        Button buttonLoadImage = (Button) findViewById(R.id.buttonLoadPicture);
        Button uploadvideo = (Button) findViewById(R.id.Upload_video);
        buttonLoadImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                videoView.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                if (videoView != null && videoView.isPlaying()) {
                    videoView.stopPlayback();
                }
                startActivityForResult(i, GIF_REQUEST);


            }
        });

        uploadvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent v1 = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                videoView.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);
                startActivityForResult(v1, VIDEO_REQUEST);
            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isPermissionNotGranted = false;
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                isPermissionNotGranted = true;
                break;
            }
        }

        if (!isPermissionNotGranted) {
            //preform action
            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
//            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            Uri uri = data.getData();
            //imageView.setImageURI(uri);
            Glide.with(MainActivity.this)
                    .load(uri)
                    .centerCrop()
                    .placeholder(R.drawable.loading).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(imageView);


            updateProfile(picturePath, requestCode);
        }


    }


    public void updateProfile(String url, final int requestCode) {
        progressBarRegion.setVisibility(View.VISIBLE);
        MultipartBody.Part image = null;
        if (!url.equals("")) {
            file = new File(url);
            Log.i("Register", "Multipart file " + file.getName());
            // create RequestBody instance from file
//                RequestBody requestFile =
//                        RequestBody.create(MediaType.parse("multipart/form-data"), file);
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("file/*"), file);
            // MultipartBody.Part is used to send also the actual file name
            String fileType = (requestCode == VIDEO_REQUEST) ? VIDEO_TYPE : IMAGE_TYPE;
            image = MultipartBody.Part.createFormData("file", "SignityXX" + fileType + file.getName(), requestFile);
        }

        Call<ResponseBody> call = ApiClient.getInstance().getApiService().updatePic(image);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
//                    {"timestamp":1559037887747,"status":200,"message":"File successfully uploaded.","data":{"name":"1559037887743_SignityXXTaco-Party.gif","parentId":0,"path":"images/1559037887743_SignityXXTaco-Party.gif","size":0}}
                    if (response.isSuccessful()) {
                        String s = response.body().string();
                        JSONObject jsonObject = new JSONObject(s);
                        String s1 = jsonObject.getJSONObject("data").getString("path");

                        Log.e(getClass().getSimpleName(), "onResponse: " + s);
                        Toast.makeText(MainActivity.this, "uploaded", Toast.LENGTH_SHORT).show();
                        getRecentFile();
                        progressBarRegion.setVisibility(View.GONE);

                    }
                } catch (Exception ie) {
                    ie.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(getClass().getSimpleName(), "onFailure: " + t.getLocalizedMessage());
                Toast.makeText(MainActivity.this, "Please check your connection", Toast.LENGTH_SHORT);
            }
        });
    }

    private void getRecentFile() {
        Call<ResponseBody> call = ApiClient.getInstance().getApiService().getRecentFile();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    String fileName = response.headers().get("fileName");
                    if (fileName.contains(IMAGE_TYPE)) {
                        imageView.setVisibility(View.VISIBLE);
                        videoView.setVisibility(View.GONE);

                        if (videoView != null && videoView.isPlaying()) {
                            videoView.stopPlayback();
                        }
                        //Image
                        String imgurl = "http://112.196.1.221:8700/images/getLatestFile";
                        //Picasso.with(BannerActivity.this).load(imgurl).placeholder(R.drawable.ic_loading).error(R.drawable.ic_error).memoryPolicy(MemoryPolicy.NO_CACHE).into(banner);
                        Glide.with(MainActivity.this)
                                .load(imgurl)
                                .centerCrop()
                                .placeholder(R.drawable.loading).error(R.drawable.ic_error).diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(imageView);
                        progressBarRegion.setVisibility(View.GONE);
                    } else {
                        //Video
                        playVideo();
                    }
                    progressBarRegion.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void playVideo() {
        imageView.setVisibility(View.GONE);
        videoView.setVisibility(View.VISIBLE);

        String imgurl = "http://112.196.1.221:8700/images/getLatestFile";
        videoView.setVideoURI(Uri.parse(imgurl));
        videoView.start();

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Toast.makeText(getApplicationContext(), "Thank You...!!!", Toast.LENGTH_LONG).show();
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(getApplicationContext(), "Oops An Error Occur While Playing Video...!!!", Toast.LENGTH_LONG).show();
                return false;
            }
        });
    }

}
